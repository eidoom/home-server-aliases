# aliases

## apt
alias aptu='sudo apt update && sudo apt upgrade -y'
alias apti='sudo apt install'
alias apts='sudo apt search'
alias aptl='sudo apt list'
alias apts='sudo apt show'
alias aptr='sudo apt remove'
alias apta='sudo apt autoremove -y'

## re-source
alias rr='source ~/.bashrc'

## directory traversal
alias ..='cd ..'
alias pd='cd -'

## git
alias gia='git add'
alias gp='git push'
alias gcm='git commit -m'
alias gcam='git commit -am'
alias gws='git status --short'

## ls
alias l='ls -A'

## docker-compose
alias dcp='docker-compose -f $HOME/dkr/docker-compose.yml'
alias dcpe='vi $HOME/dkr/docker-compose.yml'
alias dcpp='dcp pull'
alias dcpr='dcp restart'
alias dcpu='dcp up -d --remove-orphans'
alias dimp='docker image prune'
alias dcpl='dcp logs -tf --tail="50"'
alias dcps='dcp stop'
alias dcpss='dcps calibre readarr #lidarr jellyfin'
alias dcu='dcpp && dcpu && dimp -f && dcpss'

## update
alias up='aptu && dcu'

## rclone (docker)
alias rclone='dcp run --rm rclone'
### rclone sync local
function rcsl(){
    rclone sync --interactive $1:/$2 /data/$1/$2
}
function rcslf(){
    rclone sync -P $1:/$2 /data/$1/$2
}
### rclone sync remote
function rcsr(){
    rclone sync --interactive /data/$1/$2 $1:/$2
}
function rcsrf(){
    rclone sync -P /data/$1/$2 $1:/$2
}

## beets
alias beets='dcp exec beets beet'

## calibre
alias calibre='dcp exec calibre'
alias calibredb='calibre calibredb'

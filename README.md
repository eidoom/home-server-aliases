# [home-server-aliases](https://gitlab.com/eidoom/home-server-aliases)

Install:
```shell
git clone https://gitlab.com/eidoom/home-server-aliases ~/.aliases
cd ~/.aliases
./install.sh
```
